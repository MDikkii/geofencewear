package com.example.demkowmik.weargeofence;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class MainActivity extends Activity {

    private static final String TAG = MainActivity.class.getSimpleName();

    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;
    private static final String FENCE_RECEIVER_ACTION = "com.example.demkowmik.weargeofence.FENCE";
    MyFenceReceiver myFenceReceiver;
    private GeofencingClient mGeofencingClient;
    private List<Geofence> mGeofenceList;
    private Button mAddGeofencesButton;
    private Button mRemoveGeofencesButton;
    private FusedLocationProviderClient mLocationClient;
    private LocationCallback mLocationCallback;
    private Location mCurrentLocation;
    private String mLastUpdateTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAddGeofencesButton = findViewById(R.id.addButton);
        mRemoveGeofencesButton = findViewById(R.id.removeButton);


        int status = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this);
        Log.e("STATUS", String.valueOf(status));

        mGeofenceList = new ArrayList<>();
        populateGeofenceList();


        createLocationCallback();

        mGeofencingClient = LocationServices.getGeofencingClient(this);

        mLocationClient = LocationServices.getFusedLocationProviderClient(this);


        myFenceReceiver = new MyFenceReceiver();

    }


    @Override
    public void onStart() {
        super.onStart();
        if (!checkPermissions()) {
            requestPermissions();
        } else {
            registerLocationUpdates();
        }

        registerReceiver(myFenceReceiver, new IntentFilter(FENCE_RECEIVER_ACTION));
    }

    @SuppressLint("MissingPermission")
    private void addGeofences() {
        if (!checkPermissions()) {
            Log.e("PermissionProblem", "AddGeofence");
            return;
        }

        mGeofencingClient.addGeofences(getGeofencingRequest(), getGeofencePendingIntent()).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.e("OnSuccess", "goefenceAdded");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e("OnFailure", e.toString());
            }
        });
    }

    private void removeGeofences() {
        if (!checkPermissions()) {
            requestPermissions();
            return;
        }
        mGeofencingClient.removeGeofences(getGeofencePendingIntent()).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Log.e("onComplete", "removeGeofencing");
            }
        });
    }

    public void addGeofencesButtonHandler(View view) {
        if (!checkPermissions()) {

            requestPermissions();
            return;
        }
        addGeofences();

        mAddGeofencesButton.setEnabled(false);
        mRemoveGeofencesButton.setEnabled(true);
    }

    public void removeGeofencesButtonHandler(View view) {
        if (!checkPermissions()) {

            requestPermissions();
            return;
        }
        removeGeofences();

        mAddGeofencesButton.setEnabled(true);
        mRemoveGeofencesButton.setEnabled(false);
    }

    private GeofencingRequest getGeofencingRequest() {
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);
        builder.addGeofences(mGeofenceList);
        return builder.build();
    }

    private void createLocationCallback() {
        mLocationCallback = new LocationCallback() {

            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);

                mCurrentLocation = locationResult.getLastLocation();
                Log.e("New location", mCurrentLocation.toString());
                mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
            }
        };
    }

    private PendingIntent getGeofencePendingIntent() {
        Intent intent = new Intent(this, MyFenceReceiver.class);
        return PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_FINE_LOCATION);

        if (shouldProvideRationale) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.");

        } else {
            Log.i(TAG, "Requesting permission");

            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }

    private void populateGeofenceList() {
        for (Map.Entry<String, LatLng> entry : Constants.BAY_AREA_LANDMARKS.entrySet()) {

            mGeofenceList.add(new Geofence.Builder()
                    .setRequestId(entry.getKey())
                    .setCircularRegion(
                            entry.getValue().latitude,
                            entry.getValue().longitude,
                            Constants.GEOFENCE_RADIUS_IN_METERS
                    )

                    .setNotificationResponsiveness(5)
                    .setExpirationDuration(Constants.GEOFENCE_EXPIRATION_IN_MILLISECONDS)
                    .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER |
                            Geofence.GEOFENCE_TRANSITION_EXIT)
                    .build());
        }
    }


    @SuppressLint("MissingPermission")
    void registerLocationUpdates() {
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(500);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);


        mLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, null);
    }


    protected void onStop() {
        unregisterReceiver(myFenceReceiver);
        super.onStop();
    }

    public void startMap(View view) {
        Intent intent = new Intent(this, MapsActivity.class);
        startActivity(intent);
    }

//    private boolean hasGps() {
//        return getPackageManager().hasSystemFeature(PackageManager.FEATURE_LOCATION_GPS);
//    }

//    @Override
//    public void onLocationChanged(Location location) {
//        //  mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(),location.getLongitude()),10));
//        Log.e("Location", location.toString());
//        //    Log.e("IsGPS", hasGps()+"");
//    }

//
//    @Override
//    public void onStatusChanged(String s, int i, Bundle bundle) {
//        Log.e("OnStatusChanged", s);
//       // addGeofences();
//    }
//
//    @Override
//    public void onProviderEnabled(String s) {
//        Log.e("onProviderEnabled", s);
//    }
//
//    @Override
//    public void onProviderDisabled(String s) {
//        Log.e("onProviderDisables", s);
//
//    }


}
