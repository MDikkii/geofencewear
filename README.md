# README #

Simple Wear + Geofence app to show problem with Geofencing on Wear Device. 

### What is this repository for? ###

After update Google Play Sevices to version 11.7.45 Geofencing API always returns ApiException 1000, when addGeofences is called. In the background (after switching Logcat to 'No filters' option) there is a message from GeofencerStateMachine: 'W/GeofencerStateMachine: Ignoring addGeofence because network location is disabled.;. I get position on Google Maps from phone or watch itself but I can't start network provider (due to lack of this provider on Android Wear 2.0?).

https://plus.google.com/communities/113381227473021565406/stream/d980c69c-eec7-4ae2-963c-e07475b35fb7